# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Theppitak Karoonboonyanan <thep@linux.thai.net>, 2013
# Theppitak Karoonboonyanan <theppitak@gmail.com>, 2013,2017,2019,2021
# Theppitak Karoonboonyanan <theppitak@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-22 00:48+0200\n"
"PO-Revision-Date: 2021-07-21 22:48+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Thai (http://www.transifex.com/xfce/xfce-panel-plugins/language/th/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: th\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. initialize value label widget
#: ../panel-plugin/sensors-plugin.c:342 ../panel-plugin/sensors-plugin.c:457
#: ../panel-plugin/sensors-plugin.c:977
msgid "<span><b>Sensors</b></span>"
msgstr "<span><b>เซนเซอร์</b></span>"

#. output to stdout on command line, not very useful for user, except for
#. tracing problems
#: ../panel-plugin/sensors-plugin.c:854
#, c-format
msgid ""
"Sensors Plugin:\n"
"Seems like there was a problem reading a sensor feature value.\n"
"Proper proceeding cannot be guaranteed.\n"
msgstr "ปลั๊กอินเซนเซอร์:\nดูเหมือนจะมีปัญหาในการอ่านค่าบางค่าจากเซนเซอร์\nจึงไม่สามารถแน่ใจได้ว่าปลั๊กอินจะทำงานถูกต้อง\n"

#: ../panel-plugin/sensors-plugin.c:884
msgid "No sensors selected!"
msgstr "ไม่ได้เลือกเซนเซอร์ใดไว้!"

#: ../panel-plugin/sensors-plugin.c:1720
msgid "UI style:"
msgstr "รูปแบบ UI:"

#: ../panel-plugin/sensors-plugin.c:1721
msgid "_text"
msgstr "_ข้อความ"

#: ../panel-plugin/sensors-plugin.c:1722
msgid "_progress bars"
msgstr "แถบ_ปรอท"

#: ../panel-plugin/sensors-plugin.c:1723
msgid "_tachos"
msgstr "หน้าปัด_กลม"

#: ../panel-plugin/sensors-plugin.c:1757
msgid "Show _labels"
msgstr "แสดงฉ_ลาก"

#: ../panel-plugin/sensors-plugin.c:1779
msgid "_Automatic bar colors"
msgstr ""

#: ../panel-plugin/sensors-plugin.c:1781
msgid ""
"If enabled, bar colors depend on their values (normal, high, very high).\n"
"If disabled, bars use the user-defined sensor colors.\n"
"If a particular user-defined sensor color is unspecified,\n"
"the bar color is derived from the current UI style."
msgstr ""

#: ../panel-plugin/sensors-plugin.c:1808
msgid "_Show title"
msgstr "แสดง_หัวเรื่อง"

#: ../panel-plugin/sensors-plugin.c:1827
msgid "_Number of text lines:"
msgstr "_จำนวนบรรทัดของข้อความ:"

#. The Xfce 4 panel can have several rows or columns. With such a mode,
#. the plugins are allowed to span over all available rows/columns.
#. When translating, "cover" might be replaced by "use" or "span".
#: ../panel-plugin/sensors-plugin.c:1859
msgid "_Cover all panel rows/columns"
msgstr "แผ่_คลุมทุกแถว/คอลัมน์ของพาเนล"

#: ../panel-plugin/sensors-plugin.c:1879
msgid "F_ont size:"
msgstr "ข_นาดตัวอักษร:"

#: ../panel-plugin/sensors-plugin.c:1885
msgid "x-small"
msgstr "เล็กมาก"

#: ../panel-plugin/sensors-plugin.c:1886
msgid "small"
msgstr "เล็ก"

#: ../panel-plugin/sensors-plugin.c:1887
msgid "medium"
msgstr "กลาง"

#: ../panel-plugin/sensors-plugin.c:1888
msgid "large"
msgstr "ใหญ่"

#: ../panel-plugin/sensors-plugin.c:1889
msgid "x-large"
msgstr "ใหญ่มาก"

#: ../panel-plugin/sensors-plugin.c:1913
msgid "F_ont:"
msgstr "แ_บบอักษร:"

#: ../panel-plugin/sensors-plugin.c:1938
msgid "Show _Units"
msgstr "แสดง_หน่วย"

#: ../panel-plugin/sensors-plugin.c:1956
msgid "Small horizontal s_pacing"
msgstr "_ช่องว่างแนวนอนขนาดเล็ก"

#. Alpha value of the tacho coloring
#: ../panel-plugin/sensors-plugin.c:1979
msgid "Tacho color alpha value:"
msgstr "ค่าแอลฟาของสีแผนภูมิแป้นกลม:"

#. The value from HSV color model
#: ../panel-plugin/sensors-plugin.c:1992
msgid "Tacho color value:"
msgstr "ค่าสีแผนภูมิแป้นกลม:"

#: ../panel-plugin/sensors-plugin.c:2017
msgid "Suppress messages"
msgstr "ไม่ต้องแสดงข้อความ"

#: ../panel-plugin/sensors-plugin.c:2032
msgid "Suppress tooltip"
msgstr "ไม่แสดงคำแนะนำเครื่องมือ"

#: ../panel-plugin/sensors-plugin.c:2052
msgid "E_xecute on double click:"
msgstr "เ_รียกทำงานเมื่อดับเบิลคลิก:"

#: ../panel-plugin/sensors-plugin.c:2079
msgid "_View"
msgstr "การแสดง_ผล"

#: ../panel-plugin/sensors-plugin.c:2092
msgid "UI style options"
msgstr "ตัวเลือกของรูปแบบ UI"

#: ../panel-plugin/sensors-plugin.c:2121
msgid "_Miscellaneous"
msgstr "เ_บ็ดเตล็ด"

#: ../panel-plugin/sensors-plugin.c:2175 ../panel-plugin/sensors-plugin.h:31
#: ../lib/hddtemp.c:151
msgid "Sensors Plugin"
msgstr "ปลั๊กอินเซนเซอร์"

#: ../panel-plugin/sensors-plugin.c:2183
msgid "Properties"
msgstr "คุณสมบัติ"

#: ../panel-plugin/sensors-plugin.c:2209
msgid ""
"You can change a feature's properties such as name, colours, min/max value "
"by double-clicking the entry, editing the content, and pressing \"Return\" "
"or selecting a different field."
msgstr "คุณสามารถเปลี่ยนค่าต่างๆ ของเซนเซอร์แต่ละตัว เช่น ชื่อ สี ค่าต่ำสุด/สูงสุด โดยดับเบิลคลิกที่รายการที่จะเปลี่ยน แล้วแก้ไขเนื้อหา และกด \"Enter\" หรือเลือกช่องข้อมูลอื่น"

#: ../panel-plugin/sensors-plugin.c:2292
msgid "Show sensor values from LM sensors, ACPI, hard disks, NVIDIA"
msgstr "แสดงค่าเซนเซอร์จากเซนเซอร์ LM, ACPI, ฮาร์ดดิสก์, NVIDIA"

#: ../panel-plugin/sensors-plugin.c:2294
msgid "Copyright (c) 2004-2021\n"
msgstr "Copyright (c) 2004-2021\n"

#. only use this if no hddtemp sensor
#. or do only use this , if it is an lmsensors device. whatever.
#: ../lib/configuration.c:163 ../lib/hddtemp.c:358
msgid "Hard disks"
msgstr "ฮาร์ดดิสก์"

#. Note for translators: As some laptops have several batteries such as the
#. T440s,
#. there might be some perturbation with the battery name here and BAT0/BAT1
#. for
#. power/voltage. So we prepend BAT0/1 to the battery name as well, with the
#. result
#. being something like "BAT1 - 45N1127". Users can then rename the batteries
#. to
#. their own will while keeping consistency to their power/voltage features.
#. You might want to format this with a hyphen and without spacing, or with a
#. dash; the result might be BAT1–Power or whatever fits your language most.
#. Spaces allow line breaks over the tachometers.
#. You might want to format this with a hyphen and without spacing, or with a
#. dash; the result might be BAT1–Voltage or whatever fits your language most.
#. Spaces allow line breaks over the tachometers.
#: ../lib/acpi.c:317 ../lib/acpi.c:622 ../lib/acpi.c:690
#, c-format
msgid "%s - %s"
msgstr "%s-%s"

#. Power with unit Watts, not Energy with Joules or kWh
#: ../lib/acpi.c:624
msgid "Power"
msgstr "ประจุ"

#: ../lib/acpi.c:690
msgid "Voltage"
msgstr "โวลต์"

#: ../lib/acpi.c:744 ../lib/acpi.c:756
msgid "ACPI"
msgstr "ACPI"

#: ../lib/acpi.c:747
#, c-format
msgid "ACPI v%s zones"
msgstr "โซนต่างๆ ของ ACPI v%s"

#: ../lib/acpi.c:888
msgid "<Unknown>"
msgstr "<ไม่ทราบ>"

#: ../lib/nvidia.c:64
msgid "NVIDIA GPU core temperature"
msgstr "อุณหภูมิ GPU core ของ NVIDIA"

#: ../lib/nvidia.c:65
msgid "nvidia"
msgstr "nvidia"

#: ../lib/hddtemp.c:110
msgid "Don't show this message again"
msgstr "ไม่ต้องแสดงข้อความนี้อีก"

#: ../lib/hddtemp.c:357
msgid "S.M.A.R.T. harddisk temperatures"
msgstr "อุณหภูมิฮาร์ดดิสก์จาก S.M.A.R.T."

#: ../lib/hddtemp.c:527
#, c-format
msgid ""
"\"hddtemp\" was not executed correctly, although it is executable. This is most probably due to the disks requiring root privileges to read their temperatures, and \"hddtemp\" not being setuid root.\n"
"\n"
"An easy but dirty solution is to run \"chmod u+s %s\" as root user and restart this plugin or its panel.\n"
"\n"
"Calling \"%s\" gave the following error:\n"
"%s\n"
"with a return value of %d.\n"
msgstr "\"hddtemp\" ไม่ได้ทำงานอย่างถูกต้อง ถึงแม้จะสามารถเรียกขึ้นมาทำงานได้ เป็นไปได้อย่างมากว่าเกิดจากการที่ดิสก์ต้องใช้สิทธิ์ root ในการอ่านอุณหภูมิ และ \"hddtemp\" ไม่ได้ setuid เป็น root ไว้\n\nวิธีแก้ที่ง่ายแต่อาจไม่รอบคอบนักคือเรียกคำสั่ง \"chmod u+s %s\" ในฐานะผู้ใช้ root และเริ่มทำงานปลั๊กอินนี้ใหม่ หรือเริ่มทำงานพาเนลใหม่\n\nคำสั่ง \"%s\" ให้ข้อผิดพลาดต่อไปนี้:\n%s\nโดยค่าที่กลับคืนมาคือ %d\n"

#: ../lib/hddtemp.c:541 ../lib/hddtemp.c:564
msgid "Suppress this message in future"
msgstr "ไม่ต้องแสดงข้อความนี้ในอนาคต"

#: ../lib/hddtemp.c:559
#, c-format
msgid ""
"An error occurred when executing \"%s\":\n"
"%s"
msgstr "เกิดข้อผิดพลาดขณะเรียกทำงาน \"%s\":\n%s"

#: ../lib/lmsensors.c:63
msgid "LM Sensors"
msgstr "เซนเซอร์ LM"

#: ../lib/lmsensors.c:297
#, c-format
msgid "Error: Could not connect to sensors!"
msgstr "ผิดพลาด: ไม่สามารถติดต่อกับเซนเซอร์ได้!"

#: ../lib/sensors-interface.c:79
msgid "Sensors Plugin Failure"
msgstr "ปลั๊กอินเซนเซอร์ทำงานล้มเหลว"

#: ../lib/sensors-interface.c:80
msgid ""
"Seems like there was a problem reading a sensor feature value.\n"
"Proper proceeding cannot be guaranteed."
msgstr "ดูเหมือนจะมีปัญหาในการอ่านค่าบางค่าจากเซนเซอร์\nจึงไม่สามารถแน่ใจได้ว่าปลั๊กอินจะทำงานถูกต้อง"

#: ../lib/sensors-interface.c:148
msgid "Sensors t_ype:"
msgstr "_ชนิดของเซนเซอร์:"

#: ../lib/sensors-interface.c:167
msgid "Description:"
msgstr "คำบรรยาย:"

#: ../lib/sensors-interface.c:196
msgid "U_pdate interval (seconds):"
msgstr "_คาบการปรับข้อมูล (วินาที):"

#: ../lib/sensors-interface.c:231
msgid "Name"
msgstr "ชื่อ"

#: ../lib/sensors-interface.c:242
msgid "Value"
msgstr "ค่า"

#: ../lib/sensors-interface.c:248
msgid "Show"
msgstr "แสดง"

#: ../lib/sensors-interface.c:257
msgid "Color"
msgstr "สี"

#: ../lib/sensors-interface.c:268
msgid "Min"
msgstr "ต่ำสุด"

#: ../lib/sensors-interface.c:279
msgid "Max"
msgstr "สูงสุด"

#: ../lib/sensors-interface.c:311
msgid "Temperature scale:"
msgstr "หน่วยอุณหภูมิ:"

#: ../lib/sensors-interface.c:312
msgid "_Celsius"
msgstr "เซ_ลเซียส"

#: ../lib/sensors-interface.c:314
msgid "_Fahrenheit"
msgstr "_ฟาห์เรนไฮต์"

#: ../lib/sensors-interface.c:346
msgid "_Sensors"
msgstr "_เซนเซอร์"

#: ../lib/sensors-interface-common.c:70 ../lib/sensors-interface-common.c:71
msgid "No sensors found!"
msgstr "ไม่พบเซนเซอร์ใดๆ!"

#: ../lib/sensors-interface-common.c:138
#, c-format
msgid "%.0f °F"
msgstr "%.0f °F"

#: ../lib/sensors-interface-common.c:140
#, c-format
msgid "%.0f °C"
msgstr "%.0f °C"

#: ../lib/sensors-interface-common.c:144
#, c-format
msgid "%+.3f V"
msgstr "%+.3f V"

#: ../lib/sensors-interface-common.c:148
#, c-format
msgid "%+.3f A"
msgstr "%+.3f A"

#: ../lib/sensors-interface-common.c:152
#, c-format
msgid "%.0f mWh"
msgstr "%.0f mWh"

#: ../lib/sensors-interface-common.c:156
#, c-format
msgid "%.3f W"
msgstr "%.3f W"

#: ../lib/sensors-interface-common.c:160
msgid "off"
msgstr "ปิด"

#: ../lib/sensors-interface-common.c:160
msgid "on"
msgstr "เปิด"

#: ../lib/sensors-interface-common.c:164
#, c-format
msgid "%.0f rpm"
msgstr "%.0f rpm"

#: ../src/main.c:59
#, c-format
msgid ""
"Xfce4 Sensors %s\n"
"This program is published under the GPL v2.\n"
"The license text can be found inside the program's source archive or under /usr/share/apps/LICENSES/GPL_V2 or at http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt\n"
msgstr "เซนเซอร์ของ Xfce4 %s\nโปรแกรมนี้เผยแพร่ภายใต้เงื่อนไขของ GPL v2\nอ่านเนื้อหาของสัญญาอนุญาตได้ในซอร์สของโปรแกรม หรือที่ /usr/share/apps/LICENSES/GPL_V2 หรือที่ http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt\n"

#: ../src/main.c:75
#, c-format
msgid ""
"Xfce4 Sensors %s\n"
"Displays information about your hardware sensors, ACPI status, harddisk temperatures and Nvidia GPU's temperature.\n"
"Synopsis: \n"
"  xfce4-sensors [option]\n"
"where [option] is one of the following:\n"
"  -h, --help    Print this help dialog.\n"
"  -l, --license Print license information.\n"
"  -V, --version Print version information.\n"
"\n"
"This program is published under the GPL v2.\n"
msgstr "เซนเซอร์ของ Xfce4 %s\nแสดงข้อมูลเกี่ยวกับเซนเซอร์ฮาร์ดแวร์ต่างๆ ของเครื่อง, สถานะ ACPI, อุณหภูมิฮาร์ดดิสก์ และอุณหภูมิของ GPU ของ Nvidia\nรูปแบบการเรียก: \n  xfce4-sensors [ตัวเลือก]\nเมื่อ [ตัวเลือก] คือค่าใดค่าหนึ่งต่อไปนี้:\n  -h, --help    แสดงข้อความวิธีใช้นี้\n  -l, --license แสดงข้อมูลสัญญาอนุญาต\n  -V, --version แสดงข้อมูลรุ่น\n\nโปรแกรมนี้เผยแพร่ภายใต้เงื่อนไขของ GPL v2\n"

#: ../src/main.c:97
#, c-format
msgid "Xfce4 Sensors %s\n"
msgstr "เซนเซอร์ของ Xfce4 %s\n"

#: ../src/interface.c:76
msgid "_Overview"
msgstr "_ภาพรวม"

#: ../src/interface.c:97
msgid "_Tachometers"
msgstr "แ_ผนภูมิแป้นกลม"

#: ../src/interface.c:113
msgid "Sensors Viewer"
msgstr "เครื่องมือแสดงเซนเซอร์"

#. FIXME: either print nothing, or undertake appropriate action,
#. * or pop up a message box.
#: ../src/actions.c:78
#, c-format
msgid ""
"Sensors Viewer:\n"
"Seems like there was a problem reading a sensor feature value.\n"
"Proper proceeding cannot be guaranteed.\n"
msgstr "เครื่องมือแสดงเซนเซอร์\nดูเหมือนจะมีปัญหาในการอ่านค่าบางค่าจากเซนเซอร์\nจึงไม่สามารถแน่ใจได้ว่าปลั๊กอินจะทำงานถูกต้อง\n"

#: ../src/xfce4-sensors.desktop.in.h:1
msgid "Sensor Viewer"
msgstr "เครื่องมือแสดงเซนเซอร์"

#: ../src/xfce4-sensors.desktop.in.h:2
#: ../panel-plugin/xfce4-sensors-plugin.desktop.in.h:2
msgid "Show sensor values."
msgstr "แสดงค่าต่างๆ ของเซนเซอร์"

#: ../src/xfce4-sensors.desktop.in.h:3
msgid "Sensor Values Viewer"
msgstr "เครื่องมือแสดงค่าเซนเซอร์"

#: ../panel-plugin/xfce4-sensors-plugin.desktop.in.h:1
msgid "Sensor plugin"
msgstr "ปลั๊กอินเซนเซอร์"
