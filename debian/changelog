xfce4-sensors-plugin (1.4.4-1) unstable; urgency=medium

  * Team upload.

  [ Akbarkhon Variskhanov ]
  * New upstream version 1.4.4
  * debian/copyright: Update Upstream-Contact & Source
  * debian/control: Bump Standards-Version to 4.6.1
  * debian/rules: Install full upstream NEWS file
  * debian/rules: Use execute_after_dh_auto_install
  * Add upstream metadata

  [ Unit 193 ]
  * d/control: Bump B-D version constraint for libxfce4util-dev to 4.17.2.

 -- Akbarkhon Variskhanov <akbarkhon.variskhanov@gmail.com>  Sun, 18 Dec 2022 16:52:30 +0500

xfce4-sensors-plugin (1.4.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.4.3.
  * d/README.Debian: Drop, no longer useful.

 -- Unit 193 <unit193@debian.org>  Tue, 15 Feb 2022 14:29:53 -0500

xfce4-sensors-plugin (1.4.2-2) unstable; urgency=medium

  * Team upload.
  * d/control, d/rules: Disable hddtemp support. (Closes: #990051, #1001946)
  * d/control: Bump DH compat to 13.
  * d/rules: Drop '--as-needed', as this is now default in stable.

 -- Unit 193 <unit193@debian.org>  Tue, 21 Dec 2021 22:45:31 -0500

xfce4-sensors-plugin (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2
  * d/control: update standards version to 4.6.0
  * update lintian overrides

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 21 Nov 2021 11:09:28 +0100

xfce4-sensors-plugin (1.4.1-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream version 1.4.1 (Closes: #991687)
    - fix some memory leaks (Closes: #871717)
    - prevent settings dropping (Closes: #989933, #851407)
  * d/p/0001-Fix-multiple-definition-errors-under-GCC-10.0 dropped
  * d/control: update standards version to 4.5.1
  * d/watch: update version
  * d/control: R³: no
  * d/control: drop libxml2-dev and libxml-parser-perl b-deps
  * d/control: update Homepage
  * d/rules: drop manual call to dh_autoreconf

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 24 Aug 2021 21:46:29 +0200

xfce4-sensors-plugin (1.3.92-2) experimental; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 11 to 12.
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Fix field name typos in debian/copyright.
  * Update standards version to 4.4.1, no changes needed.

  [ Yves-Alexis Perez ]
  * d/watch: use uscan special strings
  * d/patches: import 108ffac from upstream to fix FTBFS (Closes: #957977)
  * lintian overrides: drop unused override
  * d/control: update standards version to 4.5.0

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 04 Nov 2020 12:03:02 +0100

xfce4-sensors-plugin (1.3.92-1) experimental; urgency=medium

  * New upstream version 1.3.92
  * use debhelper-compat b-d for dh compat mode

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 16 Jan 2019 14:02:37 +0100

xfce4-sensors-plugin (1.3.90-2) experimental; urgency=medium

  * d/control: update libsensors build-dep to libsensors-dev (Closes: #917453)
  * d/control: update standards version to 4.3.0
  * d/control: use HTTPS protocol for homepage

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 28 Dec 2018 13:42:15 +0100

xfce4-sensors-plugin (1.3.90-1) experimental; urgency=medium

  * New upstream version 1.3.90
  * d/control: update standards version to 4.2.1
  * d/watch: use HTTPS protocol
  * remove useless d/dirs file
  * use wildcard in path for lintian override

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 30 Nov 2018 14:58:24 +0100

xfce4-sensors-plugin (1.3.0-2) unstable; urgency=medium

  * enable support for NVidia cards temperature sensors (closes: #906620)
  * d/rules: reorder configure options
  * bump debhelper compat to 10
  * d/control: drop Lionel from uploaders, thanks!

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 29 Sep 2018 10:46:44 +0200

xfce4-sensors-plugin (1.3.0-1) unstable; urgency=medium

  * Moved the package to git on salsa.debian.org
  * Updated the maintainer address to debian-xfce@lists.debian.org
  * d/gbp.conf added, following DEP-14
  * New upstream version 1.3.0
  * run wrap-and-sort
  * d/control: update standards version to 4.1.4
  * d/control: update b-d for gtk-3

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 06 May 2018 19:30:12 +0200

xfce4-sensors-plugin (1.2.6-1) unstable; urgency=low

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Mateusz Łukasik ]
  * New upstream release. (Closes: #670046, #685832)
    - Remove debian/patches/fix_typo_in_fr.po.diff -- included upstream.
  * debian/control:
    - Bump standards version to 3.9.6.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 06 May 2015 22:12:35 +0200

xfce4-sensors-plugin (1.2.5-2) unstable; urgency=low

  [ Daniel Echeverry ]
  * debian/control:
    - update description to fix a lintian warning
  * debian/copyright:
    - update to DEP5 copyright format 1.0
  * debian/patches:
    - add fix_typo_in_fr.po.diff patch, fix a mistake in the french
      translation.                                              closes: #663498

  [ Yves-Alexis Perez ]
  * debian/rules:
    - enable all hardening flags.
  * debian/control:
    - remove version on xfce4-panel build-dep, stable has Xfce 4.8.
    - remove dpkg-dev build-dep, stable has hardening support.
    - update standards version to 3.9.4.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 26 May 2013 13:18:22 +0200

xfce4-sensors-plugin (1.2.5-1) unstable; urgency=low

  * New upstream release.
  * debian/patches:
    - 01_fix-hdds-detection-linux-3.x removed, included upstream.
  * debian/xfce4-sensors-plugin.lintian-overrides added, override hardening
    checks, we do enable build flags.

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 15 May 2012 23:20:16 +0200

xfce4-sensors-plugin (1.2.3-3) unstable; urgency=low

  [ Lionel Le Folgoc ]
  * debian/patches:
    - 01_fix-hdds-detection-linux-3.x.patch: fix typo. Oops! (thanks Mike
      Edwards)

  [ Yves-Alexis Perez ]
  * debian/control:
    - update standards version to 3.9.3.
  * debian/compat bumped to 9.
  * debian/rules:
    - use debhelper 9 and dpkg-dev 1.16.1 hardening support.
    - use multiarch paths.
    - enable parallel build.
    - use find to delete .la files.
    - build with --disable-static.
  * debian/control:
    - update debhelper build-dep to 9.
    - add build-dep on dpkg-de 1.16.1.
    - drop hardening-includes build-dep.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 08 Apr 2012 15:46:11 +0200

xfce4-sensors-plugin (1.2.3-2) unstable; urgency=low

  * debian/patches:
    - 01_fix-hdds-detection-linux-3.x.patch: fix incorrect test with linux
      3.x.                                                      closes: #640052

 -- Lionel Le Folgoc <mrpouit@gmail.com>  Wed, 28 Dec 2011 21:14:07 +0100

xfce4-sensors-plugin (1.2.3-1) unstable; urgency=low

  * New upstream release.
    - fix build with libnotify4.                                closes: #630310
  * debian/patches:
    - 01_fix-build-without-libnotify dropped, merged upstream.
    - 02_port-xfce-47 included upstream.
    - 03_fix-typo as well.
  * debian/control:
    - update libxfcegui4 build-dep to libxfce4ui
    - drop xfce4-dev-tools and libtool from build-deps.
    - drop cdbs build-dep.
    - add build-dep on hardening-includes.
  * debian/rules:
    - stop running xdt-autogen.
    - switch to dh7 tiny rules with overrides.
    - pick build flags from dpkg-buildflags.
    - add hardening flags to build-flags.
    - add -O1 -z,defs and --as-needed to LDFLAGS.

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 18 Jun 2011 22:46:33 +0200

xfce4-sensors-plugin (1.0.0-2) unstable; urgency=low

  [ Yves-Alexis Perez ]
  * debian/control:
    - add build-dep on libxfcegui4-dev.
    - add build-dep on xfce4-dev-tools and libtool.
    - update debhelper build-dep to 7.
    - update standards version to 3.9.2.
    - add build-dep on libnotify-dev.
    - remove Rudy, Stefan and Michael from uploaders, thanks to them.
  * debian/patches:
    - 02_port-xfce-47 added, port to 4.7 panel and link against libxfcegui4.
  * debian/rules:
    - run xdt-autogen after applying patch touching Makefile.am.
  * debian/compat bumped to 7.

  [ Lionel Le Folgoc ]
  * debian/rules: clean up autogenerated files changed by xdt-autogen.
  * Switch to 3.0 (quilt).
  * debian/patches:
    - 03_fix-typo.patch: added, fixes FTBFS with binutils-gold.  Closes: #556700
    - series: refreshed.
  * debian/control:
    - remove Simon and Emanuele from uploaders, thanks to them.
    - bump xfce4-panel-dev b-dep to (>= 4.8.0).

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 19 Apr 2011 23:25:48 +0200

xfce4-sensors-plugin (1.0.0-1) unstable; urgency=low

  [ Yves-Alexis Perez ]
  * debian/control:
    - switch section to xfce.
    - update standards version to 3.8.4.
    - update debhelper build-dep to 7.
    - build-dep on libnotify-dev.
  * debian/watch edited to track Xfce archive reorganisation.
  * debian/source/format added with 3.0 (quilt)
  * debian/rules:
    - drop simple-patchsys include.
    - explicitly enable notifications.
  * debian/compat updated to 7.

  [ Evgeni Golov ]
  * Fix Vcs-* fields, they were missing 'trunk' in the path.

  [ Lionel Le Folgoc ]
  * New upstream release.
  * debian/patches/01_fix_segfault_at_startup.patch: dropped, included
    upstream.
  * debian/control: added myself to uploaders.
  * debian/rules: fix typo in configure options.
  * debian/patches/01_fix-build-without-libnotify.patch: added, fixes FTBFS
    (see Xfce #6349).

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 23 Apr 2010 00:16:37 +0200

xfce4-sensors-plugin (0.10.99.6-2) unstable; urgency=low

  * Merger of patches from Ubuntu to Debian:
  * debian/patches:
    - 01_fix_segfault_at_startup.patch added.
      Resolves segfaults at plugin initialization		closes: #519181
  * debian/control:
    - Added myself to uploaders

 -- Michael Casadevall <mcasadevall@debian.org>  Wed, 06 May 2009 17:38:32 -0400

xfce4-sensors-plugin (0.10.99.6-1) unstable; urgency=low

  * New upstream release.
  * debian/patches:
    - 01_fix-acpi-fan-infos dropped.
  * debian/control:
    - add build dep on libtool.
    - add ${misc:Depends} to depends.
  * debian/rules:
    - remove {.a,.la} files in /usr/lib/xfce4/modules too.
    - don't run dh_makeshlibs on libxfce4sensors.so.1.0.0 (private).
    - install xfce4-sensors.1 in xfce4-sensors-plugin.
  * debian/xfce4-sensors.1:
    - add stub manpage for xfce4-sensors.

 -- Yves-Alexis Perez <corsac@debian.org>  Mon, 23 Feb 2009 22:30:43 +0100

xfce4-sensors-plugin (0.10.99.5~svn-r4998-2) unstable; urgency=low

  [ Stefan Ott ]
  * debian/control: update e-mail address

  [ Yves-Alexis Perez ]
  * debian/patches:
    - 01_fix-acpi-fan-infos added, don't crash plugin when refreshing fan
      state. Thanks Daniel Gibson for debugging this and cooking the patch.
                                                                closes: #491480
    - 02_fix-acpi-battery-path added, don't add twice “battery” in path.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 06 Aug 2008 08:27:55 +0200

xfce4-sensors-plugin (0.10.99.5~svn-r4998-1) unstable; urgency=low

  * New upstream svn snapshot.
    - warn only once about hddtemp unavailable.        closes: #483831, #488151
  * don't build-dep on hddtemp on kfreebsd and hurd, where it's unavailable.
  * debian/control:
    - correct typo.                               closes: #474697
    - update standards version to 3.8.0.
  * debian/patches:
    - 01_dont-look-at-floppy-disk dropped, the plugin now uses a whitelist.

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 27 Jun 2008 09:12:24 +0200

xfce4-sensors-plugin (0.10.99.4~svn-r3775-2) unstable; urgency=low

  * debian/patches: 01_dont-look-at-floppy-disk added, so hddtemp support
    doesn't try to find a temp sensor in a floppy disk.         closes: #463815
  * debian/copyright: update format to use © sign.


 -- Yves-Alexis Perez <corsac@debian.org>  Thu, 28 Feb 2008 21:15:23 +0100

xfce4-sensors-plugin (0.10.99.4~svn-r3775-1) unstable; urgency=low

  * New upstream svn snapshot.
    add libsensors 3 support.                                   closes: #458978
  * debian/rules: enable configure flags explicitely.
  * debian/control: build-dep on hddtemp so its path is correctly detected.

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 04 Jan 2008 11:53:43 +0100

xfce4-sensors-plugin (0.10.99.2-1) unstable; urgency=low

  [ Emanuele Rocca ]
  * Recommend hddtemp and add README.Debian explaining that it needs to be
    installed with the SUID bit set.
  * Add Vcs-* headers to debian/control

  [ Simon Huggins ]
  * debian/control: Move fake Homepage field to a real one now dpkg
    supports it.

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/copyright: slightly updated.
  * debian/control:
    - updated standard versions to 3.7.3.
    - updated my email addres to the debian.org one.
  * debian/rules: remove /usr/lib/xfce4/panel-plugins if it's empty.

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 08 Dec 2007 19:52:37 +0100

xfce4-sensors-plugin (0.10.0-2) unstable; urgency=low

  (Yves-Alexis Perez)
  * debian/control:
    - added dep on lm-sensors so plugin can work.
  (Stefan Ott)
    - updated the url
  (Simon Huggins)
    - updated build-deps to 4.4.1.

 -- Yves-Alexis Perez <corsac@corsac.net>  Sun, 29 Apr 2007 20:32:28 +0100

xfce4-sensors-plugin (0.10.0-1) unstable; urgency=low

  (Yves-Alexis Perez)
  * New upstream release.
  * debian/control: updated build-deps to 4.3.99.2 (Xfce4.4rc2).

 -- Yves-Alexis Perez <corsac@corsac.net>  Thu, 16 Nov 2006 19:16:56 +0100

xfce4-sensors-plugin (0.9.0-2) unstable; urgency=low

  * Updated build-deps to 4.3.90.2 (Xfce 4.4 Beta 2).
  * Updated standards version to 3.7.2.

 -- Yves-Alexis Perez <corsac@corsac.net>  Wed, 26 Jul 2006 16:34:05 +0100

xfce4-sensors-plugin (0.9.0-1) unstable; urgency=low

  * (Emanuele Rocca)
    * New upstream release
  * (Yves-Alexis Perez)
    * Removed explicit dependency on xfce4-panel (managed by shlibs)

 -- Emanuele Rocca <ema@debian.org>  Wed, 03 May 2006 19:17:15 +0100

xfce4-sensors-plugin (0.7.0-1) unstable; urgency=low

  * New upstream version (Closes: #326636)
  * debian/patches: treeview patch no longer needed
  * debian/control: dpatch build dependency removed

 -- Stefan Ott <stefan@desire.ch>  Sun, 18 Sep 2005 17:55:34 +0200

xfce4-sensors-plugin (0.6.1-2) unstable; urgency=low

  * debian/control:
    + Added libtool and autoconf build-dependencies (Closes: #324099)
    + Added dpatch build-dependency
    + Removed warning about rendering the panel unusable
  * debian/patches/01_treeview.dpatch: Added listview patch to prevent a
    segfault when no sensor is present

 -- Stefan Ott <stefan@desire.ch>  Mon, 22 Aug 2005 01:03:49 +0200

xfce4-sensors-plugin (0.6.1-1) unstable; urgency=low

  * Rudy Godoy and Stefan Ott
    - Initial release (Closes: #320685)

 -- Stefan Ott <stefan@desire.ch>  Tue,  2 Aug 2005 15:58:43 +0200
